#include "Game.h"
#include "DxLib.h"
#include "KeyBoard.h"

Game::Game(ISceneChanger* changer) : BaseScene(changer) {
}



void Game::Initialize() {

}

void Game::Finalize() {

}

void Game::Update() {
	KeyBoard_Update();

	if (KeyBoard_Get(KEY_INPUT_SPACE) != 0) {
		mSceneChanger->ChangeScene(eScene_Over);
	}
}

void Game::Draw() {
	DrawString(360, 240, "�Q�[�����", GetColor(255, 255, 255));
}