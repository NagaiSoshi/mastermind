#ifndef DEF_CHANGER_H
#define DEF_CHANGER_H

typedef enum {
    eScene_Menu,    //メニュー画面
    eScene_Game,    //ゲーム画面
    eScene_Config,  //設定画面
    eScene_Title,   //タイトル画面
    eScene_Over,    //ゲームオーバー
    eScene_Crear,   //ゲームクリア
    
    eScene_None,    //無し
} eScene;

//シーンを変更するためのインターフェイスクラス
class ISceneChanger {
public:
    virtual ~ISceneChanger() = 0;
    virtual void ChangeScene(eScene NextScene) = 0;//指定シーンに変更する
};


#endif