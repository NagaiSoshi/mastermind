#ifndef ENUM_OVER_H
#define ENUM_OVER_H

#include "BaseScene.h"

class Over : BaseScene {

public:
	Over(ISceneChanger* changer);
	void Initialize() override;
	void Finalize() override;
	void Update() override;
	void Draw() override;
};

#endif