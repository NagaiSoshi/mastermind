#include "DxLib.h"
#include "KeyBoard.h"

static int Key[256];

void KeyBoard_Update() {
	char tmpKey[256];
	GetHitKeyStateAll(tmpKey);
	for (int i = 0; i < 256;i++) {
		if (tmpKey[i] != 0) {
			Key[i]++;
		}
		else {
			Key[i] = 0;
		}
	}
}

int KeyBoard_Get(int KeyCode) {
	return Key[KeyCode];
}