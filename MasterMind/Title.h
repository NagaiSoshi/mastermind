#ifndef ENUM_TITLE_H
#define ENUM_TITLE_H

#include "BaseScene.h"

class Title : public BaseScene {

public:
	Title(ISceneChanger* changer);
	void Initialize() override;
	void Finalize() override;
	void Update() override;
	void Draw() override;
};




#endif