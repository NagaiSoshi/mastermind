#include "DxLib.h"
#include "Over.h"
#include "KeyBoard.h"

Over::Over(ISceneChanger* changer) : BaseScene(changer) {
}

void Over::Initialize() {

}

void Over::Finalize() {

}

void Over::Update() {
	KeyBoard_Update();
	if (KeyBoard_Get(KEY_INPUT_ESCAPE) != 0) {
		mSceneChanger->ChangeScene(eScene_Title);
	}
}

void Over::Draw() {
	DrawString(320, 240, "ゲームオーバー", GetColor(255, 255, 255));
}