#include "Title.h"
#include "DxLib.h"
#include "KeyBoard.h"

Title::Title(ISceneChanger* changer) : BaseScene(changer) {
}

void Title :: Initialize() {

}

void Title::Finalize() {

}

void Title::Update() {
	KeyBoard_Update();
	if (KeyBoard_Get(KEY_INPUT_RETURN) != 0) {
		mSceneChanger->ChangeScene(eScene_Game);
	}
}

void Title::Draw() {
	DrawString(320, 240, "タイトル画面",GetColor(255,255,255));
	DrawString(320, 280, "Press Enter to Start", GetColor(255, 255, 255));
}